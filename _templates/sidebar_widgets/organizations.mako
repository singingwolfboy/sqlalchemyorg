<p>SQLAlchemy is used by organizations such as:
<ul>
    <li><a href="http://reddit.com">reddit</a></li>
    <li><a href="http://www.openstack.org/">The OpenStack Project</a></li>
    <li><a href="http://www.lolapps.com">lolapps</a></li>
    <li><a href="http://www.freshbooks.com/">Freshbooks</a></li>
    <li><a href="http://fedoraproject.org/wiki/Infrastructure/Services">Fedora Project</a></li>
    <li><a href="http://www.creatureshop.com/">Jim Henson's Creature Shop</a></li>
    <li><a href="http://lexmachina.stanford.edu/">Lexmachina at Stanford University</a></li>
</ul>
</p>
<a href="/trac/wiki/SAApps">more...</a>
