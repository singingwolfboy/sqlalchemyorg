<%page args="site_base, docs_base"/>

<div id="footer">

    <div class="pypowered"><a href="http://www.python.org/" title="The Python Language Site"><img src="${site_base}/img/python-logo.gif" width="88" height="30" alt="Python" class="pypowered" /></a></div>

    <div class="copyright">
        <p>Website content copyright &copy; by SQLAlchemy authors and contributors.
            SQLAlchemy and its documentation are licensed under the MIT license.</p>
        <p>SQLAlchemy is a trademark of Michael Bayer.  mike(&amp;)zzzcomputing.com
        All rights reserved. </p>
        <p>Website generation by <a href="http://www.blogofile.com">Blogofile</a> and
                <a href="http://www.makotemplates.org">Mako Templates for Python</a>.</p>
     </div>

    <br clear="all" />

</div>

